import useFetch from "../utils/fetchItems";
import { makeStyles} from "@material-ui/core";


const MyBudget = () => {
    const url = `http://localhost:4000/api/budget`
    const {data, error, status} = useFetch(url);
    const useStyles = makeStyles((theme) => ({
        main: {
            fontFamily: "Roboto, Sans-serif",
            marginTop:"120px"
        }
    }));
    const classes = useStyles();

    return (
        <div className={classes.main}>
            <h2>My Budget</h2>
            <p>₹ {data}</p>
        </div>
    )
}

export default MyBudget;