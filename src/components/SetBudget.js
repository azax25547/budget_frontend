import {useFormik} from "formik";
import {Button,  makeStyles, TextField} from "@material-ui/core";
import * as yup from 'yup';
import axios from 'axios';
import CustomizedSnackbars from "../utilComponets/Snackbar";
import useSnackBar from "../utils/useSnackBar";

const validationSchema = yup.object({
    amount: yup
        .number('Enter your Monthly Budget')
        .min(1, 'Monthly Budget must not be less than 1')
        .required('Budget is required'),

});

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    },
    margin: {
        margin: theme.spacing(1),
    },

    main: {
        marginTop: "150px",
        marginBottom: "150px"
    }
}));

const BASE_URL = "http://localhost:4000/api/";

const SetBudget = () => {
    const classes = useStyles();
    // const history = useHistory();

    const [open, msg, status, handleClick, setSnackBar] =  useSnackBar();


    const formik = useFormik({
        initialValues: {
            amount: ''
        },
        validationSchema,
        onSubmit: async values => {
            try {
                const res = await axios.post(`${BASE_URL}/set`,values);
                if(res?.data?.done) {
                    setSnackBar(true, "Budget Created Successfully!","success");
                }
                else{
                    setSnackBar(true, "Unable to create Budget.","error");
                }
                // history.push("/actions")
            } catch(err) {
                setSnackBar(true, err.toString(),"error");
            }
            formik.values.amount = '';
        }
    });
    return (
        <div className={classes.main}>
            <h1>Set your monthly Budget & Start!</h1>
            <form className={classes.root} autoComplete={"off"} onSubmit={formik.handleSubmit}>
                <TextField
                    onChange={formik.handleChange}
                    id="amount"
                    name={"amount"}
                    label="Amount"
                    type={"Number"}
                    value={formik.values.amount}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    placeholder={"₹"}
                    variant="outlined"
                    error={formik.touched.amount && Boolean(formik.errors.amount)}
                    helperText={formik.touched.amount && formik.errors.amount}
                />
                <br />
                <Button color="primary" variant="contained"  type="submit">
                    Submit
                </Button>
            </form>
            <CustomizedSnackbars open={open} message={msg} status={status} handleClose={handleClick}/>
        </div>
    )
}


export default SetBudget;