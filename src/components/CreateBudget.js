import {
    Button, Divider,
    makeStyles,
    MenuItem,
    TextField
} from "@material-ui/core";
import {useFormik} from "formik";
import * as yup from 'yup';
import React, {useCallback, useEffect, useState} from 'react';
import axios from "axios";
import {useHistory} from "react-router-dom";
import CustomizedSnackbars from "../utilComponets/Snackbar";
import useSnackBar from "../utils/useSnackBar";
import useFetch from "../utils/fetchItems";

const BASE_URL = 'http://localhost:4000/api'
const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    },
    margin: {
        margin: theme.spacing(1),
    },

    main: {
        marginTop: "150px",
        marginBottom: "150px"
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },

}));

const categoryValidationSchema = yup.object({
    category: yup
            .string("Please Enter a Category").required('Category is Required')
})

const budgetValidationSchema = yup.object({
    category: yup
        .string("Please Enter a Category")
        .required('Category is Required'),
    amount:  yup
        .number('Enter your Monthly Budget')
        .min(1, 'Monthly Budget must not be less than 1')
        .required('Budget is required'),
})

const CreateBudget = () => {
    const history = useHistory();
    const url = `http://localhost:4000/api/category`
    const {data} = useFetch(url);
    const [open, msg, status, handleClick, setSnackBar] =  useSnackBar();

    const ctgFormik = useFormik({
        initialValues: {
            category: ''
        },
        validationSchema: categoryValidationSchema,
        onSubmit: values => {
            axios.post(`${BASE_URL}/create/category`, values)
                .then((res) => {
                    if(res.data.done)
                        setSnackBar(true, res.data.message, 'success')
                    else
                        setSnackBar(true, res.data.message, 'warning')
                }).catch(err => {
                    setSnackBar(true, err.toString(), 'error')
            })
            ctgFormik.values.category = ''
        }
    });

    const budgetFormik = useFormik({
        initialValues: {
            category: '',
            amount: 0
        },
        validationSchema: budgetValidationSchema,
        onSubmit(values) {
            axios.post(`${BASE_URL}/create`,values, )
                .then((res) => {
                    if(res.data.done) {
                        console.log(res.data.message);
                        history.push("/budgets")
                    }

                })
                .catch(err => {
                    if(err.response){
                        setSnackBar(true, err.response.data.message, 'error')
                        console.log(err.response.data.message)
                    }
                })
                    budgetFormik.values.category = ''
                    budgetFormik.values.amount = 0;

        }
    })

    const classes = useStyles();
    return (
        <div style={{marginTop: "75px"}}>
            {/* Category */}
            <div>
                <h2>Create A Category</h2>
                <form className={classes.root} autoComplete={"off"} onSubmit={ctgFormik.handleSubmit}>
                    <TextField
                        fullwidth={"true"}
                        onChange={ctgFormik.handleChange}
                        id="amount"
                        name={"category"}
                        label="Category"
                        type={"Text"}
                        value={ctgFormik.values.category}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        placeholder={"Category"}
                        variant="outlined"
                        error={ctgFormik.touched.category && Boolean(ctgFormik.errors.category)}
                        helperText={ctgFormik.touched.category && ctgFormik.errors.category}
                    />
                    <br/>
                    <Button color="primary" variant="contained" type="submit" >
                        CREATE
                    </Button>
                </form>
            </div>
            {/* Budget */}
            <Divider variant={'middle'} style={{ marginTop: "10px"}}/>
            <div>
                <h2>Create an Envelope</h2>
                <form className={classes.root} autoComplete={"off"} onSubmit={budgetFormik.handleSubmit}>
                    <TextField
                        onChange={budgetFormik.handleChange}
                        id="category"
                        name={"category"}
                        label="Category"
                        select
                        value={budgetFormik.values.category}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        placeholder={"₹"}
                        variant="outlined"
                        error={budgetFormik.touched.category && Boolean(budgetFormik.errors.category)}
                        helperText={budgetFormik.touched.category && budgetFormik.errors.category}
                        disabled={data.length === 0}
                    >
                        {data.map((item,index) => {
                            return (
                                <MenuItem key={index} value={item}>
                                    {item}
                                </MenuItem>
                            )
                        })}

                    </TextField>
                    <br/>
                    <TextField
                        onChange={budgetFormik.handleChange}
                        id="amount"
                        name={"amount"}
                        label="Amount"
                        type={"Number"}
                        value={budgetFormik.values.amount}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        placeholder={"₹"}
                        variant="outlined"
                        error={budgetFormik.touched.amount && Boolean(budgetFormik.errors.amount)}
                        helperText={budgetFormik.touched.amount && budgetFormik.errors.amount}
                    />

                    <br/>
                    <Button color="primary" variant="contained"  type="submit" disabled={data.length === 0}>
                        CREATE
                    </Button>
                 </form>
            </div>
            <CustomizedSnackbars open={open} message={msg} status={status} handleClose={handleClick}/>
        </div>
    )
}

export default CreateBudget;