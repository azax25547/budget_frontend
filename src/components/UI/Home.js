import {Container, Grid, makeStyles} from "@material-ui/core";
import About from "./About";
import SetBudget from "../SetBudget";
import CustomizedSnackbars from "../../utilComponets/Snackbar";

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        fontFamily: "Roboto, sans-serif"
    },
    main: {
        marginTop: "50px",

    },
}))

const Home = () => {
    const classes = useStyles();
    return (
        <Container>
            <div className={classes.paper}>
                <Grid container spacing={2} className={classes.main}>
                    <Grid item xs={12} md={6} sm={6} lg={6}>
                        <About />
                    </Grid>
                    <Grid item xs={12} md={6} sm={6} lg={6}>
                        <SetBudget />
                    </Grid>
                </Grid>
            </div>
        </Container>
    )
}

export default Home;