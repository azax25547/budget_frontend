import {makeStyles, Button, Grid} from "@material-ui/core";
import {Link} from "react-router-dom";

const useStyles = makeStyles((theme) => ({

    heading: {
        fontSize: "3.5em",
        color: "palevioletred"
    },
    paraDesc: {
        fontSize: "1em",
        textAlign: "justify",
        marginTop: "20px",
        padding: "20px",

    },
    ulLists: {
        listStyleType: "circle",
    },
    link: {
        color:"white",
        textDecoration: "none"
    }

}))

const About = () => {
    const classes = useStyles();
    return (
        <div>
            <div className={classes.heading}>
                My Budgets
            </div>
            <div className={classes.paraDesc}>
                <p>
                    My Budgets is a simple Budget Tracking Application, Where you can simply create Envelopes on various spends and then can track them, how you are utilizing your budgets. This application is built as per <strong>Envelope Budgeting</strong> system.
                </p>
                <h2>Features:</h2>
                <ul className={classes.ulLists}>
                    <li> Set your Monthly Budget. </li>
                    <li> Create Envelopes on your monthly Spends. </li>
                    <li> Update your Envelopes. </li>
                    <li> Delete your Envelopes. </li>
                    <li> Transfer from one to other Envelopes. </li>
                    <li> Track your Monthly budget. </li>
                </ul>
            </div>

            <Grid container spacing={2} >
                <Grid item xs={12} >
                    <Button variant={"contained"} color={"primary"}>
                        <Link to={"/actions"} className={classes.link}>
                            Let's Create Envelopes
                        </Link>
                    </Button>
                </Grid>
                <Grid item xs={12} >
                    <Button variant={"contained"} color={"primary"}>
                        <Link to={"/budgets"} className={classes.link}>
                            View My Envelopes
                        </Link>
                    </Button>
                </Grid>

            </Grid>

        </div>
    )
}

export default About;