import {
    List,
    ListItem,
    ListItemText,
    makeStyles,
    Typography
} from "@material-ui/core";
import {Link} from "react-router-dom";
import ExitToAppTwoToneIcon from "@material-ui/icons/ExitToAppTwoTone";
import useFetch from "../utils/fetchItems";

const Budgets = () => {
    const url = `http://localhost:4000/api/get/`
    const {data} = useFetch(url);

    const useStyles = makeStyles((theme) => ({
        root: {
            width: '100%',
            maxWidth: '36ch',
            backgroundColor: theme.palette.background.paper,
        },
        inline: {
            display: 'inline',
        },
        main: {
            fontFamily: "Roboto, Sans-serif"
        },link : {
            textDecoration: "none",
        },

    }));
    const classes = useStyles();
    return (
           <div className={classes.main}>
               <Link to={"/actions"} className={classes.link}>
                   <ExitToAppTwoToneIcon color={"primary"} fontSize={'large'}/>
               </Link>
               <h2>Envelopes...</h2>
               <List className={classes.root}>
                   {data.map((item, index) => {
                       return (
                           <ListItem alignItems="flex-start" key={index}>
                               <ListItemText
                                   primary={<Link to={`/budget/${item.id}`}>{`${item.category}`}</Link>}
                               />
                           </ListItem>
                       )})}
               </List>
           </div>


    )
}


export default Budgets;