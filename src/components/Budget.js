import ExitToAppTwoToneIcon from "@material-ui/icons/ExitToAppTwoTone";
import {Link} from "react-router-dom";
import {Grid, makeStyles} from "@material-ui/core";
import { useParams } from 'react-router-dom';
import useFetch from "../utils/fetchItems";

const Budget = () => {
    const useStyles = makeStyles((theme) => ({
        paper: {
            padding: theme.spacing(2),
            overflow: "hidden"
        },
        main: {
            fontFamily: "Roboto, sans-serif",
        },
        link : {
            textDecoration: "none",
        },
        back : {
            width:"100%",
            height:0,
            paddingBottom: "100%",
            position:"relative"
        }
    }))
    const classes = useStyles();
    let { id } = useParams();
    const url = `http://localhost:4000/api/get/${id}`
    const {data} = useFetch(url)
    return (

        <div className={classes.main}>

            <Link to={"/budgets"} className={classes.link}>
                <ExitToAppTwoToneIcon color={"primary"} fontSize={'large'}/>
            </Link>
            <Grid container spacing={0} direction={"column"} alignItems={"center"} justifyContent={"center"} style={{ minHeight: "90vh"}}>
                <Grid item xs={3}>
                    <>
                        <h1>{data.category}</h1>
                        <p>Lorem ipsum dolor sit amet</p>
                        <strong>Amount: </strong> ₹ {data.amount}
                    </>
                </Grid>

            </Grid>


        </div>
    )
}

export default Budget;