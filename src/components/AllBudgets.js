import {Container, Grid, makeStyles} from "@material-ui/core";
import {Link} from "react-router-dom";
import CreateBudget from "./CreateBudget";
import TransferBudgets from "./TransferBudgets";

import ExitToAppTwoToneIcon from '@material-ui/icons/ExitToAppTwoTone';
import MyBudget from "./MyBudget";

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(2),
        overflow: "hidden"
    },
    main: {
        fontFamily: "Roboto, sans-serif",
        marginTop: "10px",
        textAlign: 'center'
    },
    link : {
        textDecoration: "none",
    },
    back : {
        width:"100%",
        height:0,
        paddingBottom: "100%",
        position:"relative"
    }
}))

const AllBudgets = () => {

    const classes = useStyles();
    return (
        <Container>
            <div className={classes.paper}>
                <Link to={"/"} className={classes.link}>
                   <ExitToAppTwoToneIcon color={"primary"} fontSize={'large'}/>
                </Link>
                <Grid container spacing={2} className={classes.main} >
                    <Grid item xs={12} md={6} sm={6} lg={6} >
                       <MyBudget />
                        <TransferBudgets />

                    </Grid>
                    <Grid item xs={12} md={6} sm={6} lg={6} >
                        <CreateBudget />
                    </Grid>
                </Grid>
            </div>
        </Container>
    )
}

export default AllBudgets;