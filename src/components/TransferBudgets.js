import {Button, makeStyles, MenuItem, TextField} from "@material-ui/core";
import React from "react";
import * as yup from "yup";
import {useFormik} from "formik";
import useFetch from "../utils/fetchItems";
import useSnackBar from "../utils/useSnackBar";
import axios from "axios";
import CustomizedSnackbars from "../utilComponets/Snackbar";

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    },
    margin: {
        margin: theme.spacing(1),
    },

    main: {
        marginTop: "50px",

    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },

}));

const transferValidationSchema = yup.object({
    from: yup
        .string("Please Select a Category")
        .required('Category is Required')
        .test(
            "Not Equal","Must Not Be Equal to Destination Envelope",
            function (item) {
                return item !== this.parent.to
            }
        ),
    to: yup
        .string("Please Select a Category")
        .required('Category is Required'),
    amount:  yup
        .number('Enter the Amount you want to Transfer')
        .min(1, 'Transfer amount must not be less than 1')
        .required('Amount is required'),
})

const TransferBudgets = () => {
    const classes = useStyles();
    const url = `http://localhost:4000/api/get/`
    const BASE_URL = `http://localhost:4000/api`;
    const {data} = useFetch(url);

    const [open, msg, status, handleClick, setSnackBar] =  useSnackBar();

    const budgetFormik = useFormik({
        initialValues: {
            from: '',
            to:'',
            amount: 0
        },
        validationSchema: transferValidationSchema,
        async onSubmit(values) {
            try {
                const currentAmount = data.filter(item => item.category === values.from)[0].amount;
                if(currentAmount > values.amount){
                    const res = await axios.post(`${BASE_URL}/transfer`,values);

                    if(res?.data?.done) {
                        setSnackBar(true, res.data.message ,"success");
                    }
                    else{
                        setSnackBar(true, res.data.message,"error");
                    }
                }else{
                    setSnackBar(true, "Transfer Amount is more for the Selected Envelope.","warning");
                }

                // history.push("/actions")
            } catch(err) {
                setSnackBar(true, err.toString(),"error");
            }
            // budgetFormik.values.amount = '';
        }
    })

    return (
        <div className={classes.main}>
            <h2>Transfer Your Envelopes</h2>
            <form className={classes.root} autoComplete={"off"} onSubmit={budgetFormik.handleSubmit}>
                <TextField
                    onChange={budgetFormik.handleChange}
                    id="from"
                    name={"from"}
                    label="From"
                    select
                    value={budgetFormik.values.from}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    variant="outlined"
                    error={budgetFormik.touched.from && Boolean(budgetFormik.errors.from)}
                    helperText={budgetFormik.touched.from && budgetFormik.errors.from}
                    disabled={data.length === 0}
                >
                    {data.map((item,index) => {
                        return (
                            <MenuItem key={index} value={item.category}>
                                {item.category}
                            </MenuItem>
                        )
                    })}

                </TextField>

                <TextField
                    onChange={budgetFormik.handleChange}
                    id="to"
                    name={"to"}
                    label="To"
                    select
                    value={budgetFormik.values.to}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    variant="outlined"
                    error={budgetFormik.touched.to && Boolean(budgetFormik.errors.to)}
                    helperText={budgetFormik.touched.to && budgetFormik.errors.to}
                    disabled={data.length === 0}
                >
                    {data.map((item,index) => {
                        return (
                            <MenuItem key={index} value={item.category}>
                                {item.category}
                            </MenuItem>
                        )
                    })}

                </TextField>
                <br/>
                <TextField
                    onChange={budgetFormik.handleChange}
                    id="amount"
                    name={"amount"}
                    label="Amount"
                    type={"Number"}
                    value={budgetFormik.values.amount}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    placeholder={"₹"}
                    variant="outlined"
                    error={budgetFormik.touched.amount && Boolean(budgetFormik.errors.amount)}
                    helperText={budgetFormik.touched.amount && budgetFormik.errors.amount}
                    disabled={data.length === 0}
                />

                <br/>
                <Button color="primary" variant="contained"  type="submit" disabled={data.length === 0}>
                    CREATE
                </Button>
            </form>
            <CustomizedSnackbars open={open} message={msg} status={status} handleClose={handleClick}/>

        </div>
    )
}

export default TransferBudgets;