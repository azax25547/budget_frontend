import {useState} from "react";

export default function () {

    const [open, setOpen] = useState(false)
    const [msg, setMessage] = useState("")
    const [status, setStatus] = useState("")

    const handleClick = () => {
        setOpen(false);
        setMessage("")
        setStatus("")
    }

    const setSnackBar = (isOpen, msg, sts) => {
        setOpen(isOpen);
        setMessage(msg)
        setStatus(sts);
    }

    return [open, msg, status, handleClick, setSnackBar];

}