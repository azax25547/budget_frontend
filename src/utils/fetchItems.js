import {useCallback, useEffect, useState, useRef, useReducer} from "react";
import axios from "axios";


export default function(url) {
    const cache = useRef({});

    const initialState = {
        status: 'idle',
        error: null,
        data: [],
    };

    const [state, dispatch] = useReducer((state, action) => {
        switch (action.type) {
            case 'FETCHING':
                return { ...initialState, status: 'fetching' };
            case 'FETCHED':
                return { ...initialState, status: 'fetched', data: action.payload };
            case 'FETCH_ERROR':
                return { ...initialState, status: 'error', error: action.payload };
            default:
                return state;
        }
    }, initialState);

    useEffect(() => {
        let cancelRequest = false;
        if (!url) return;

        const getItems = async () => {
            dispatch({ type: 'FETCHING' });
            if(cache.current[url]){
                const data = cache.current[url];
                dispatch({ type: 'FETCHED', payload: data });

            }else {
                try {
                    const res = await axios.get(url);
                    const data = res.data.data;
                    cache.current[url] = data;
                    if (cancelRequest) return;
                    dispatch({ type: 'FETCHED', payload: data });
                } catch(error) {
                    if (cancelRequest) return;
                    dispatch({ type: 'FETCH_ERROR', payload: error.message });
                }
            }
            // console.log(cache.current);
            // console.log(state)
        }

        getItems();
        return function cleanup() {
            cancelRequest = true;
        };
    },[state.status, url])
    return state;
}