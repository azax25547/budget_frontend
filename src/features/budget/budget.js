import { createSlice } from '@reduxjs/toolkit'

export const budgetSlice = createSlice({
    name:'budget',
    initialState : {
        myBudget:0,
        categories: [],
        envelopes: []
    },
    reducers: {
        getCategories: (state) => {
            state.categories = ["Hello", "There"]
        },
        getEnvelopes: (state) => {
            state.envelopes = ["Hello", "There"]
        },
        getMyBudget: (state) => {
            state.myBudget = "items";
        }
    }
})

export const { getCategories, getEnvelopes, getMyBudget } = budgetSlice.actions;
export default budgetSlice.reducer