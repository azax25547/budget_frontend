import { Container} from "@material-ui/core";
import Home from "./components/UI/Home";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import AllBudgets from "./components/AllBudgets";
import Error404 from "./components/UI/Error404";
import Budgets from "./components/Budgets";
import Budget from "./components/Budget";

function App() {
  return (
<Router>
    <Switch>
        <Route path="/" exact>
            <Home />
        </Route>
        <Route path="/actions">
            <AllBudgets />
        </Route>
        <Route path="/budgets">
            <Budgets />
        </Route>
        <Route path="/budget/:id">
            <Budget />
        </Route>
        <Route path="*">
            <Error404 />
        </Route>
    </Switch>
</Router>

  );
}

export default App;
