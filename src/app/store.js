import { configureStore } from '@reduxjs/toolkit'
import budgetReducer from '../features/budget/budget';

export default configureStore({
    reducer: {
        budget: budgetReducer
    },
})